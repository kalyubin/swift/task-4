enum Messengers: String {
    case Telegram, VKontakte, WhatsApp, Viber, Facebook
}

enum Trees: String {
    case oak, brich, pine, maple, willow, aspen, fir
}

enum DifficultyLevel: Int {
  case easy = 1, medium, hard, impossible
}

enum ChineseZodiac: Int {
  case rat = 1
  case bull = 2
  case tiger = 3
  case rabbit = 4
  case dragon = 5
  case snake = 6
  case horse = 7
  case goat = 8
  case monkey = 9
  case rooster = 10
  case dog = 11
  case pig = 12
}

enum Gender {
    case male, female
}

enum AgeCategory {
    case young, adult, senior
}

enum Experience {
    case inexperienced, lessThan1Year, between1And3Years, between3And5Years, MoreThan5Years
}

struct Employee {
    var Name: String
    var Gender: Gender
    var AgeCategory: AgeCategory
    var Experience: Experience
}

enum RainbowColors: Int {
    case red = 1, orange, yellow, green, lightBlue, blue, violet
}

enum Vegetables: String {
  case tomato, cucumber, carrot, potato, aubergine, onion
}

enum Color: String {
  case red, green, orange, yellow, violet, white
}

func vegetableColors() {
  let veg = [Vegetables.tomato, Vegetables.cucumber, Vegetables.carrot, Vegetables.potato, Vegetables.aubergine, Vegetables.onion]
  let colors = [Color.red, Color.green, Color.orange, Color.yellow, Color.violet, Color.white]
  
  for index in 0..<min(veg.count, colors.count) {
    print("\(veg[index].rawValue) is \(colors[index].rawValue)")
  }
}

vegetableColors()

print()

enum Score: String {
  case A = "Very good"
  case B = "Good"
  case C = "Satisfactory"
  case D = "Poor"
  case E = "Very poor"
  case F = "Awful"
}

func getNumericalGrade(score: Score) {
  switch score {
  case .A:
    print("5")
  case .B:
    print("4")
  case .C:
    print("3")
  case .D:
    print("2")
  case .F:
    print("1")
  }
}

getNumericalGrade(score: Score.A)
getNumericalGrade(score: Score.B)
getNumericalGrade(score: Score.C)
getNumericalGrade(score: Score.D)
getNumericalGrade(score: Score.F)

print()

enum Car: String {
  case LadaVesta
  case UAZPatriot
  case ChevroletNiva
  case VAZ2107
  case RenaultLogan
}

func carsInGarage(cars: [Car]) {
  print("In the garage:")
  for car in cars {
    print(" - \(car.rawValue)")
  }
}

let inGarage = [Car.LadaVesta, Car.VAZ2107, Car.ChevroletNiva]
carsInGarage(cars: inGarage)